package ctp.trade;

import ctp.thostapi.*;

import java.util.HashMap;
import java.util.Vector;

/**
 * 自定义TradeSpi类，实现认证、登陆、结算单确认、报单录入等回调功能
 */
public class MyTradeSpi extends CThostFtdcTraderSpi {
    private final MyTradeApi m_traderapi;

    MyTradeSpi(MyTradeApi traderapi) {
        m_traderapi = traderapi;
    }

    /**
     * 当客户端与交易托管系统建立起通信连接时（还未登录前），该方法被调用。
     * 本方法在完成初始化后调用，可以在其中完成用户认证。
     */
    @Override
    public void OnFrontConnected() {
        System.out.println("On Front Connected");
        m_traderapi.ReqAuthenticate();  //用户认证
    }

    /**
     * 客户端认证响应,当执行ReqAuthenticate后，该方法被调用。
     * 可以在其中完成用户登陆。
     */
    @Override
    public void OnRspAuthenticate(CThostFtdcRspAuthenticateField pRspAuthenticateField, CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
        if (pRspInfo != null && pRspInfo.getErrorID() != 0) {
            System.out.printf("OnRspAuthenticate ErrorID[%d] ErrMsg[%s]\n", pRspInfo.getErrorID(), pRspInfo.getErrorMsg());
            return;
        }
        System.out.println("OnRspAuthenticate success!!!");
        m_traderapi.ReqUserLogin();     //用户登陆
    }

    /**
     * 登录请求响应，当执行ReqUserLogin后，该方法被调用。
     * 可以在其中完成结算单确认。
     */
    @Override
    public void OnRspUserLogin(CThostFtdcRspUserLoginField pRspUserLogin, CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
        if (pRspInfo != null && pRspInfo.getErrorID() != 0) {
            System.out.printf("OnRspUserLogin ErrorID[%d] ErrMsg[%s]\n", pRspInfo.getErrorID(), pRspInfo.getErrorMsg());

            return;
        }
        System.out.println("Login success!!!");
        m_traderapi.ReqSettlementInfoConfirm();     //结算单确认
    }

    /**
     * 投资者结算结果确认响应，当执行ReqSettlementInfoConfirm后，该方法被调用。
     * 可以在其中完成报单录入、撤单等操作。
     */
    @Override
    public void  OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField pSettlementInfoConfirm, CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast)
    {
        if (pRspInfo != null && pRspInfo.getErrorID() != 0)
        {
            System.out.printf("OnRspSettlementInfoConfirm ErrorID[%d] ErrMsg[%s]\n", pRspInfo.getErrorID(), pRspInfo.getErrorMsg());
            return;
        }
        System.out.println("Settlement info confirm success!!!");
        // m_traderapi.ReqOrderInsert();   //报单录入
        // m_traderapi.ReqOrderDelete("SHFE", "141792");    //撤单
    }

    /**
     * 报单录入请求响应，当执行ReqOrderInsert后有字段填写不对之类的CTP报错则通过此接口返回。
     *
     */
    @Override
    public void OnRspOrderInsert(CThostFtdcInputOrderField pInputOrder, CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast)
    {
        if (pRspInfo != null && pRspInfo.getErrorID() != 0)
        {
            System.out.printf("OnRspOrderInsert ErrorID[%d] ErrMsg[%s]\n", pRspInfo.getErrorID(), pRspInfo.getErrorMsg());
            return;
        }
        if (pInputOrder != null)
        {
            System.out.println(pInputOrder.getInstrumentID() + "order insert success!!!");
        }
        else
        {
            System.out.println("NULL obj");
        }

    }

    /**
     * 报单录入错误回报，当执行ReqOrderInsert后有字段填写不对之类的CTP报错则通过此接口返回
     *
     */
    @Override
    public void OnErrRtnOrderInsert(CThostFtdcInputOrderField pInputOrder, CThostFtdcRspInfoField pRspInfo)
    {
        System.out.printf("OnErrRtnOrderInsert ErrorID[%d] ErrMsg[%s]\n", pRspInfo.getErrorID(), pRspInfo.getErrorMsg());
        System.out.println(pInputOrder.getInstrumentID() + "order insert error!!!");
    }

    /**
     * 报单通知，当执行ReqOrderInsert后并且报出后，收到返回则调用此接口，私有流回报。
     *
     */
    public void OnRtnOrder(CThostFtdcOrderField pOrder)
    {
        System.out.println("order return success!!!");
        System.out.println(pOrder.getOrderSysID().length());
        System.out.println("交易所：" + pOrder.getExchangeID() + ",委托状态：" + pOrder.getOrderStatus() + ",委托系统号：" + pOrder.getOrderSysID());
    }

    /**
     * 成交通知，报单发出后有成交则通过此接口返回。私有流
     *
     */
    public void OnRtnTrade(CThostFtdcTradeField pTrade)
    {
        System.out.println("trade return success!!!");
    }

    /**
     * 报单操作请求响应，当执行ReqOrderAction后有字段填写不对之类的CTP报错则通过此接口返回。
     *
     */
    public void OnRspOrderAction(CThostFtdcInputOrderActionField pInputOrderAction, CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast)
    {
        if (pRspInfo != null && pRspInfo.getErrorID() != 0)
        {
            System.out.printf("OnRspOrderAction ErrorID[%d] ErrMsg[%s]\n", pRspInfo.getErrorID(), pRspInfo.getErrorMsg());
            return;
        }
        if (pInputOrderAction != null)
        {
            System.out.println(pInputOrderAction.getInstrumentID() + "order action " + pInputOrderAction.getActionFlag() + " success!!!");
        }
        else
        {
            System.out.println("NULL obj");
        }

    }

    /**
     * 报单操作错误回报，当执行ReqOrderAction后有字段填写不对之类的CTP报错则通过此接口返回。
     *
     */
    public void OnErrRtnOrderAction(CThostFtdcOrderActionField pOrderAction, CThostFtdcRspInfoField pRspInfo)
    {
        System.out.printf("OnErrRtnOrderAction ErrorID[%d] ErrMsg[%s]\n", pRspInfo.getErrorID(), pRspInfo.getErrorMsg());
        System.out.println(pOrderAction.getInstrumentID() + "order action " + pOrderAction.getActionFlag() +" error!!!");
    }

}
