package ctp.trade;

public class main {
    static {
        // 加载动态依赖库，根据本地操作系统使用dll或so
        System.loadLibrary("./lib/thosttraderapi_se");
        System.loadLibrary("./lib/thostmduserapi_se");
        System.loadLibrary("./lib/thostapi_wrap");
    }


    public static void main(String[] args) {
        MyTradeApi traderApi = new MyTradeApi();
        traderApi.start();
        traderApi.join();
        return;
    }
}
